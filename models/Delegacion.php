<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "delegacion".
 *
 * @property int $id_delegacion
 * @property string $poblacion
 * @property string $direccion
 * @property string $nombre
 *
 * @property Trabajadores[] $trabajadores
 */
class Delegacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delegacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['poblacion', 'nombre'], 'string', 'max' => 31],
            [['direccion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_delegacion' => 'Id Delegacion',
            'poblacion' => 'Poblacion',
            'direccion' => 'Direccion',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajadores()
    {
        return $this->hasMany(Trabajadores::className(), ['delegacion' => 'id_delegacion']);
    }
}
